package com.haihack.alarmer.models;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by haihack on 14/04/2016.
 */
public class MathProblem {

   private int mMax = 99;
   private int mMin = 11;
   private int mAnswer = -1;
   private ArrayList<Integer> mParts;
   private MathProblem.Operator[] mOperators;
   public MathProblem(Alarm.Difficulty diff) {
      Random random = new Random();
      int numParts = 0;
      switch (diff) {
         case EASY:
            numParts = 2;
            mOperators = new Operator[]{Operator.ADD};
            break;
         case MEDIUM:
            numParts = 3;
            mOperators = new Operator[]{Operator.ADD, Operator.ADD};
            break;
         case HARD:
            numParts = 3;
            mOperators = new Operator[]{Operator.MULTIPLY, Operator.ADD};
            break;
      }

      mParts = new ArrayList<>(numParts);
      for (int i = 0; i < numParts; i++) {
         int generateNum = random.nextInt(mMax - mMin + 1) + mMin;
         if (diff == Alarm.Difficulty.HARD && i == 1) {
            generateNum = random.nextInt(7) + 2;
         }
         mParts.add(i, generateNum);
      }

      ArrayList<Object> combinedParts = new ArrayList<>();
      for (int i = 0; i < numParts; i++) {
         combinedParts.add(mParts.get(i));
         if (i < numParts - 1) {
            combinedParts.add(mOperators[i]);
         }
      }

      while (combinedParts.contains(Operator.MULTIPLY)) {
         int i = combinedParts.indexOf(Operator.MULTIPLY);
         mAnswer = (int) combinedParts.get(i - 1) * (int) combinedParts.get(i + 1);
         for (int r = 0; r < 2; r++) {
            combinedParts.remove(i - 1);
         }
         combinedParts.set(i - 1, mAnswer);
      }

      while (combinedParts.contains(Operator.ADD)) {
         int i = combinedParts.indexOf(Operator.ADD);
         mAnswer = (int) combinedParts.get(i - 1) + (int) combinedParts.get(i + 1);
         for (int r = 0; r < 2; r++) {
            combinedParts.remove(i - 1);
         }
         combinedParts.set(i - 1, mAnswer);
      }
//      while (combinedParts.contains(Operator.SUBTRACT)) {
//         int i = combinedParts.indexOf(Operator.SUBTRACT);
//         answer = (int) combinedParts.get(i - 1) - (int) combinedParts.get(i + 1);
//         for (int r = 0; r < 2; r++) {
//            combinedParts.remove(i - 1);
//         }
//         combinedParts.set(i - 1, answer);
//      }

   }

   public String getAnswer() {
      return mAnswer + "";
   }

   @Override
   public String toString() {
      StringBuilder problemBuilder = new StringBuilder();
      for (int i = 0; i < mParts.size(); i++) {
         problemBuilder.append(mParts.get(i));
         if (i < mParts.size() - 1) {
            problemBuilder.append(mOperators[i].toString());
         }
      }
      return problemBuilder.toString();
   }

   enum Operator {
      ADD, SUBTRACT, MULTIPLY, DIVIDE;

      @Override
      public String toString() {
         String string = null;
         switch (ordinal()) {
            case 0:
               string = "+";
               break;
            case 1:
               string = "-";
               break;
            case 2:
               string = "x";
               break;
            case 3:
               string = "/";
               break;
         }
         return string;
      }
   }
}
