package com.haihack.alarmer.models;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import android.widget.Toast;

import com.haihack.alarmer.db.DatabaseConstants;
import com.haihack.alarmer.db.DatabaseHelper;
import com.haihack.alarmer.receivers.AlarmReceiver;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by haihack on 06/04/2016.
 */
@Parcel
public class Alarm {

   //private fields cannot be detected during annotation
   int mId;
   String mLabel;
   boolean mIsVibrate = true;
   boolean mIsActive = true;
   //   boolean isRepeat = false;
   String mGoesOffTimeInString;
   Day[] mRepeatDays;
   Uri mRingtoneUri = Settings.System.DEFAULT_ALARM_ALERT_URI;
   Difficulty mDifficulty = Difficulty.MEDIUM;
   // empty constructor needed by the Parceler library
   public Alarm() {
   }

   public int getId() {
      return mId;
   }

   public void setId(int id) {
      this.mId = id;
   }

   public String getLabel() {
      return mLabel;
   }

   public void setLabel(String label) {
      this.mLabel = label;
   }

   public boolean isVibrate() {
      return mIsVibrate;
   }

   public void setIsVibrate(boolean isVibrate) {
      this.mIsVibrate = isVibrate;
   }

   public boolean isActive() {
      return mIsActive;
   }

   public void setIsActive(boolean isActive) {
      this.mIsActive = isActive;
   }

   public String getGoesOffTimeInString() {
      return mGoesOffTimeInString;
   }

   public void setGoesOffTimeInString(String goesOffTimeInString) {
      this.mGoesOffTimeInString = goesOffTimeInString;
   }

   public Uri getRingtoneUri() {
      return mRingtoneUri;
   }

   public void setRingtoneUri(Uri ringtoneUri) {
      this.mRingtoneUri = ringtoneUri;
   }

   public String getRingtoneName(Context context) {
      if (mRingtoneUri == null) {
         return "Silent";
      }
      Ringtone tone = RingtoneManager.getRingtone(context, mRingtoneUri);
      return tone.getTitle(context);
   }

   public Difficulty getDifficulty() {
      return mDifficulty;
   }

   public void setDifficulty(Difficulty difficulty) {
      this.mDifficulty = difficulty;
   }

   public Day[] getRepeatDays() {
      return mRepeatDays;
   }

   public void setRepeatDays(Day[] repeatDays) {
      this.mRepeatDays = repeatDays;
   }

   public String getRepeatDaysInString() {
      if (mRepeatDays == null) {
         return "None";
      }
      if (mRepeatDays.length == 7) {
         return "Everyday";
      } else {
         String label = "";
         for (Alarm.Day repeatDay : mRepeatDays) {
            label += repeatDay.toString() + ",";
         }
         return label.substring(0, label.length() - 1);
      }
   }

   public Calendar getGoesOffTime() {
      String[] timePieces = mGoesOffTimeInString.split(":");

      Calendar goesOffTime = Calendar.getInstance();
      goesOffTime.set(Calendar.HOUR_OF_DAY,
            Integer.parseInt(timePieces[0]));
      goesOffTime.set(Calendar.MINUTE, Integer.parseInt(timePieces[1]));
      goesOffTime.set(Calendar.SECOND, 0);

      if (goesOffTime.before(Calendar.getInstance())) {
         goesOffTime.add(Calendar.DAY_OF_MONTH, 1);
      }
      if (getRepeatDays() != null) {
         while (!Arrays.asList(getRepeatDays()).contains(Day.values()[goesOffTime.get(Calendar.DAY_OF_WEEK) - 1])) {
            goesOffTime.add(Calendar.DAY_OF_MONTH, 1);
         }
      }
      return goesOffTime;
   }

   public String getTimeUntilNextAlarmMessage() {
      long timeDifference = getGoesOffTime().getTimeInMillis() - System.currentTimeMillis();
      long days = timeDifference / (1000 * 60 * 60 * 24);
      long hours = timeDifference / (1000 * 60 * 60) - (days * 24);
      long minutes = timeDifference / (1000 * 60) - (days * 24 * 60) - (hours * 60);
      //long seconds = timeDifference / (1000) - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);
      String msg = "Alarm set for ";
      if (days > 0) {
         msg += String.format(
               "%d days, %d hours and %d minutes from now.", days, hours, minutes);
      } else {
         if (hours > 0) {
            msg += String.format("%d hours and %d minutes from now.", hours, minutes);
         } else {
            if (minutes > 0) {
               msg += String.format("%d minutes from now.", minutes);
            } else {
               msg += String.format("less than 1 minute from now.");
            }
         }
      }
      return msg;
   }

   public void schedule(Context context) {
      Intent intent = new Intent(context, AlarmReceiver.class);
      intent.putExtra(AlarmReceiver.EXTRA_ALARM, Parcels.wrap(this));

      PendingIntent pendingIntent = PendingIntent
            .getBroadcast(context, getId(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
      AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
      alarmManager.set(AlarmManager.RTC_WAKEUP, getGoesOffTime().getTimeInMillis(), pendingIntent);

      setIsActive(true);
      DatabaseHelper.getInstance(context).updateData(this, DatabaseConstants.TABLE_ALARM);

      Toast.makeText(context, getTimeUntilNextAlarmMessage(), Toast.LENGTH_SHORT).show();
   }

   public void cancelSchedule(Context context) {
      Intent intent = new Intent(context, AlarmReceiver.class);
      PendingIntent pIntent = PendingIntent.getBroadcast(context,
            getId(), intent, 0);
      AlarmManager alarmManager = (AlarmManager) context
            .getSystemService(Context.ALARM_SERVICE);
      alarmManager.cancel(pIntent);

      setIsActive(false);
      DatabaseHelper.getInstance(context).updateData(this, DatabaseConstants.TABLE_ALARM);
   }

   public enum Difficulty {
      EASY,
      MEDIUM,
      HARD;

      @Override
      public String toString() {
         switch (this.ordinal()) {
            case 0:
               return "Easy";
            case 1:
               return "Medium";
            case 2:
               return "Hard";
         }
         return super.toString();
      }
   }

   public enum Day {
      SUNDAY,
      MONDAY,
      TUESDAY,
      WEDNESDAY,
      THURSDAY,
      FRIDAY,
      SATURDAY;

      @Override
      public String toString() {
         switch (this.ordinal()) {
            case 0:
               return "Mon";
            case 1:
               return "Tue";
            case 2:
               return "Wed";
            case 3:
               return "Thu";
            case 4:
               return "Fri";
            case 5:
               return "Sat";
            case 6:
               return "Sun";
         }
         return super.toString();
      }
   }

}
