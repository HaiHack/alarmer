package com.haihack.alarmer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.haihack.alarmer.R;
import com.haihack.alarmer.db.DatabaseConstants;
import com.haihack.alarmer.models.Alarm;

import org.parceler.Parcels;

import butterknife.Bind;
import butterknife.OnClick;

public class UpdateAlarmActivity extends AddAlarmActivity {
   public static final String EXTRA_UPDATE_ACTION = "action";
   public static final int ACTION_EDIT = 1;
   public static final int ACTION_DELETE = 2;
   @Bind(R.id.btnDelete)
   Button mBtnDelete;
   private Alarm mAlarm;

   @OnClick(R.id.btnDelete)
   public void deleteAlarm() {
      mDb.deleteData(getAlarm().getId(), DatabaseConstants.TABLE_ALARM);
      Intent returnIntent = new Intent();
      returnIntent.putExtra(EXTRA_UPDATE_ACTION, ACTION_DELETE);
      setResult(Activity.RESULT_OK, returnIntent);
      finish();
   }

   @OnClick(R.id.btnOk)
   public void saveAlarm() {
      mDb.updateData(getAlarm(), DatabaseConstants.TABLE_ALARM);
      Intent returnIntent = new Intent();
      returnIntent.putExtra(EXTRA_UPDATE_ACTION, ACTION_EDIT);
      returnIntent.putExtra(EXTRA_RESULT, Parcels.wrap(mAlarm));
      setResult(Activity.RESULT_OK, returnIntent);
      finish();
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      mAlarm = Parcels.unwrap(getIntent().getParcelableExtra(MainActivity.EXTRA_ALARM));
      mBtnDelete.setVisibility(View.VISIBLE);
   }

   @Override
   protected void onStart() {
      super.onStart();
      mFrgmPref.updateViewContent(mAlarm);
   }

}
