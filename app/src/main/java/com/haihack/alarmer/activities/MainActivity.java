package com.haihack.alarmer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.haihack.alarmer.R;
import com.haihack.alarmer.adapters.AlarmAdapter;
import com.haihack.alarmer.db.DatabaseConstants;
import com.haihack.alarmer.db.DatabaseHelper;
import com.haihack.alarmer.models.Alarm;
import com.haihack.alarmer.utilities.RecyclerTouchListener;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

   public static final String EXTRA_ALARM = "alarm";
   public static final int REQUEST_ADD_NEW_ALARM = 901;
   public static final int REQUEST_UPDATE_ALARM = 902;
   @Bind(R.id.rvAlarm)
   RecyclerView mRvAlarm;
   private AlarmAdapter mAdapter;
   private ArrayList<Alarm> mAlarms;
   private int mClickedPosition;

   @OnClick(R.id.btnAddAlarm)
   public void addAlarm() {
      Intent i = new Intent(this, AddAlarmActivity.class);
      startActivityForResult(i, REQUEST_ADD_NEW_ALARM);
   }

   @Override
   protected void onResume() {
      super.onResume();
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);

      mAlarms = (ArrayList<Alarm>) (ArrayList<?>) DatabaseHelper.getInstance(this)
            .getAllData(DatabaseConstants.TABLE_ALARM);
      mAdapter = new AlarmAdapter(this, mAlarms);
      LinearLayoutManager layoutManager = new LinearLayoutManager(this);
      mRvAlarm.setLayoutManager(layoutManager);
      mRvAlarm.setAdapter(mAdapter);
      mRvAlarm.addOnItemTouchListener(
            new RecyclerTouchListener(mContext, new RecyclerTouchListener.ClickListener() {
               @Override
               public void onClick(View view, int position) {
                  mClickedPosition = position;
                  Alarm alarm = mAlarms.get(position);
                  Intent i = new Intent(mContext, UpdateAlarmActivity.class);
                  i.putExtra(EXTRA_ALARM, Parcels.wrap(alarm));
                  startActivityForResult(i, REQUEST_UPDATE_ALARM);
               }
            }));
   }

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
      if (resultCode == RESULT_OK) {
         if (requestCode == REQUEST_ADD_NEW_ALARM) {
            Alarm alarm = Parcels.unwrap(intent.getParcelableExtra(AddAlarmActivity.EXTRA_RESULT));
            mAlarms.add(0, alarm);
            mAdapter.notifyItemInserted(0);

         } else if (requestCode == REQUEST_UPDATE_ALARM) {
            if (intent.getIntExtra(UpdateAlarmActivity.EXTRA_UPDATE_ACTION, -1)
                  == UpdateAlarmActivity.ACTION_DELETE) {
               mAlarms.remove(mClickedPosition);
               mAdapter.notifyItemRemoved(mClickedPosition);
            } else if (intent.getIntExtra(UpdateAlarmActivity.EXTRA_UPDATE_ACTION, -1)
                  == UpdateAlarmActivity.ACTION_EDIT) {
               Alarm updatedAlarm = Parcels.unwrap(intent.getParcelableExtra(AddAlarmActivity.EXTRA_RESULT));
               mAlarms.set(mClickedPosition, updatedAlarm);
               mAdapter.notifyItemChanged(mClickedPosition);
            }
         }
      }
   }

}
