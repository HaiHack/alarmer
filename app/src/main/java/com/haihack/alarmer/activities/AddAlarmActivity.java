package com.haihack.alarmer.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.provider.Settings;

import com.haihack.alarmer.R;
import com.haihack.alarmer.db.DatabaseConstants;
import com.haihack.alarmer.db.DatabaseHelper;
import com.haihack.alarmer.models.Alarm;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.OnClick;

public class AddAlarmActivity extends BaseActivity {

   public static final String PREF_KEY_DIFF = "diff";
   public static final String PREF_KEY_LABEL = "label";
   public static final String PREF_KEY_REPEAT = "repeat";
   public static final String PREF_KEY_RINGTONE = "ringtone";
   public static final String PREF_KEY_TIME = "time";
   public static final String PREF_KEY_VIBRATE = "vibrate";

   public static final String EXTRA_RESULT = "result";

   protected MyPreferenceFragment mFrgmPref;
   protected DatabaseHelper mDb;

   @OnClick(R.id.btnOk)
   public void addAlarm() {
      Alarm alarm = mFrgmPref.mAlarm;

      int id = (int) mDb.insertData(alarm, DatabaseConstants.TABLE_ALARM);
      alarm.setId(id);
      alarm.schedule(this);

      Intent returnIntent = new Intent();
      returnIntent.putExtra(EXTRA_RESULT, Parcels.wrap(alarm));
      setResult(Activity.RESULT_OK, returnIntent);
      finish();
   }

   @OnClick(R.id.btnCancel)
   public void cancel() {
      finish();
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_add_alarm);
      getSupportActionBar().setTitle("Set Alarm");

      mDb = DatabaseHelper.getInstance(this);

      FragmentManager frgmManager = getFragmentManager();
      FragmentTransaction transaction = frgmManager.beginTransaction();
      mFrgmPref = new MyPreferenceFragment();
      transaction.replace(R.id.fragment_container, mFrgmPref);
      transaction.commit();
   }

   public Alarm getAlarm() {
      return mFrgmPref.mAlarm;
   }

   public static class MyPreferenceFragment extends PreferenceFragment
         implements TimePickerDialog.OnTimeSetListener,
         Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {

      private static final int REQUEST_SELECT_REPEAT_MODE = 111;
      private TimePickerDialog mPicker;
      private Preference mPrefTime;
      private Preference mPrefRepeat;
      private Preference mPrefRingtone;
      private EditTextPreference mPrefLabel;
      private CheckBoxPreference mPrefVibrate;
      private ListPreference mPrefDiff;
      private Alarm mAlarm = new Alarm();

      @Override
      public void onCreate(final Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         addPreferencesFromResource(R.xml.preference);

         mPrefTime = findPreference(PREF_KEY_TIME);
         mPrefTime.setOnPreferenceClickListener(this);
         updateTimeInPrefTime();

         mPrefRepeat = findPreference(PREF_KEY_REPEAT);
         mPrefRepeat.setOnPreferenceClickListener(this);

         mPrefRingtone = findPreference(PREF_KEY_RINGTONE);
         Ringtone tone = RingtoneManager.getRingtone(getActivity(), Settings.System.DEFAULT_ALARM_ALERT_URI);
         mPrefRingtone.setSummary(tone.getTitle(MyPreferenceFragment.this.getActivity()));
         mPrefRingtone.setOnPreferenceChangeListener(this);

         mPrefLabel = (EditTextPreference) findPreference(PREF_KEY_LABEL);
         mPrefLabel.setOnPreferenceChangeListener(this);

         mPrefVibrate = (CheckBoxPreference) findPreference(PREF_KEY_VIBRATE);
         mPrefVibrate.setOnPreferenceChangeListener(this);

         mPrefDiff = (ListPreference) findPreference(PREF_KEY_DIFF);
         mPrefDiff.setOnPreferenceClickListener(this);
         mPrefDiff.setOnPreferenceChangeListener(this);

      }

      public void updateTimeInPrefTime() {
         Calendar c = Calendar.getInstance();
         int hour = c.get(Calendar.HOUR_OF_DAY);
         int min = c.get(Calendar.MINUTE);
         mPrefTime.setSummary(((hour < 10) ? "0" + hour : hour + "")
               + ":" + ((min < 10) ? "0" + min : min + ""));
         mAlarm.setGoesOffTimeInString(mPrefTime.getSummary().toString());
      }

      @Override
      public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
         String min = (minute < 10) ? "0" + minute : minute + "";
         String hour = (hourOfDay < 10) ? "0" + hourOfDay : hourOfDay + "";
         mPrefTime.setSummary(hour + ":" + min);
         mAlarm.setGoesOffTimeInString(mPrefTime.getSummary().toString());
      }

      @Override
      public boolean onPreferenceChange(Preference pref, Object o) {
         switch (pref.getKey()) {
            case PREF_KEY_RINGTONE:
               if (o.toString().equals("")) {
                  pref.setSummary("Silent");
                  mAlarm.setRingtoneUri(null);
               } else {
                  Uri uri = Uri.parse(o.toString());
                  mAlarm.setRingtoneUri(uri);
                  pref.setSummary(mAlarm.getRingtoneName(getActivity()));
               }
               break;
            case PREF_KEY_DIFF:
               int index = Integer.parseInt(o.toString());
               pref.setSummary(Alarm.Difficulty.values()[index].toString());
               mAlarm.setDifficulty(Alarm.Difficulty.values()[index]);
               ((ListPreference) pref).setValueIndex(index);
               break;
            case PREF_KEY_LABEL:
               EditTextPreference prefLabel = (EditTextPreference) pref;
               prefLabel.setSummary(prefLabel.getEditText().getText());
               mAlarm.setLabel(prefLabel.getEditText().getText().toString());
               break;
            case PREF_KEY_VIBRATE:
               mAlarm.setIsVibrate((boolean) o);
               break;
            default:
               break;
         }
         return true;
      }

      @Override
      public boolean onPreferenceClick(Preference pref) {
         switch (pref.getKey()) {
            case PREF_KEY_TIME:
               if (mPicker != null) {
                  if (!mPicker.isHidden()) {
                     mPicker.dismiss();
                  }
               }
               String[] time = pref.getSummary().toString().split(":");
               mPicker = TimePickerDialog.newInstance(MyPreferenceFragment.this,
                     Integer.valueOf(time[0]), Integer.valueOf(time[1]), 0, true);
               mPicker.show(getFragmentManager(), "tagPicker");
               break;
            case PREF_KEY_REPEAT:
               Intent i = new Intent(getActivity(), RepeatActivity.class);
               startActivityForResult(i, REQUEST_SELECT_REPEAT_MODE);
               break;
            default:
               break;
         }
         return false;
      }

      @Override
      public void onActivityResult(int requestCode, int resultCode, Intent intent) {
         super.onActivityResult(requestCode, resultCode, intent);
         if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_REPEAT_MODE) {
               ArrayList<Integer> repeatDayOrdinals =
                     intent.getIntegerArrayListExtra(RepeatActivity.EXTRA_RESULT);

               Alarm.Day[] repeatDays = new Alarm.Day[repeatDayOrdinals.size()];
               for (int i = 0; i < repeatDayOrdinals.size(); i++) {
                  repeatDays[i] = Alarm.Day.values()[repeatDayOrdinals.get(i)];
               }
               mAlarm.setRepeatDays(repeatDays);
               mPrefRepeat.setSummary(mAlarm.getRepeatDaysInString());
            }
         } else {
            mAlarm.setRepeatDays(null);
            mPrefRepeat.setSummary("None");
         }
      }

      public void updateViewContent(Alarm alarm) {
         this.mAlarm = alarm;
         mPrefTime.setSummary(alarm.getGoesOffTimeInString());

         if (alarm.getRepeatDays() != null) {
            mPrefRepeat.setSummary(alarm.getRepeatDaysInString());
         }

         mPrefRingtone.setSummary(alarm.getRingtoneName(getActivity()));

         mPrefVibrate.setChecked(alarm.isVibrate());

         mPrefDiff.setSummary(alarm.getDifficulty().toString());
         mPrefDiff.setValueIndex(alarm.getDifficulty().ordinal());

         mPrefLabel.setSummary(alarm.getLabel());
      }
   }
}
