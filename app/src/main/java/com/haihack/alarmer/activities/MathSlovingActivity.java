package com.haihack.alarmer.activities;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.haihack.alarmer.R;
import com.haihack.alarmer.models.Alarm;
import com.haihack.alarmer.models.MathProblem;
import com.haihack.alarmer.receivers.AlarmReceiver;

import org.parceler.Parcels;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MathSlovingActivity extends Activity {

   @Bind(R.id.tvAnswer)
   TextView mTvAnswer;
   @Bind(R.id.tvCalculus)
   TextView mTvCalculus;
   private Alarm mAlarm;
   private StringBuffer mAnswer = new StringBuffer("");
   private MathProblem mProblem;
   private MediaPlayer mPlayer;

   @OnClick({R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5,
         R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9, R.id.btn0})
   public void inputNumber(Button btn) {
      mAnswer.append(btn.getText().toString());
      mTvAnswer.setText(mAnswer);
   }

   @OnClick(R.id.btnClear)
   public void clearLastNumber() {
      if (mAnswer.length() > 0) {
         mAnswer.deleteCharAt(mAnswer.length() - 1);
         mTvAnswer.setText(mAnswer);
      }
   }

   @OnClick(R.id.btnSubmit)
   public void submit() {
      if (mTvAnswer.getText().toString().equals(mProblem.getAnswer())) {
         mPlayer.stop();
         finish();
      }
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      requestWindowFeature(Window.FEATURE_NO_TITLE);
      setFinishOnTouchOutside(false);
      setContentView(R.layout.activity_math_sloving);
      ButterKnife.bind(this);

      mAlarm = Parcels.unwrap(getIntent().getParcelableExtra(AlarmReceiver.EXTRA_ALARM));
      mProblem = new MathProblem(mAlarm.getDifficulty());
      mTvCalculus.setText(mProblem.toString());

      try {
         mPlayer = new MediaPlayer();
         mPlayer.setDataSource(this, mAlarm.getRingtoneUri());
         final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
         if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
            mPlayer.setAudioStreamType(AudioManager.STREAM_RING);
            mPlayer.setLooping(true);
            mPlayer.prepare();
            mPlayer.start();
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   @Override
   public void onBackPressed() {

   }

}
