package com.haihack.alarmer.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.haihack.alarmer.R;
import com.haihack.alarmer.utilities.AppLogger;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by haihack on 06/07/2015.
 */
public class BaseActivity extends AppCompatActivity {
   @Nullable
   @Bind(R.id.app_bar)
   protected Toolbar mToolbar;
   protected Context mContext;

   private boolean mShouldShowHome = true;
   private ActionBar mActionBar;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      mContext = this;
   }

   @Override
   public void setContentView(int layoutResID) {
      super.setContentView(layoutResID);
      ButterKnife.bind(this);

      setupToolbar();

      mActionBar = getSupportActionBar();
      if (mActionBar != null) {
         mActionBar.setTitle(null);

         if (mShouldShowHome) {
            //bien nut home thanh nut up
            mActionBar.setDisplayHomeAsUpEnabled(true);
            //show logo
            mActionBar.setDisplayShowHomeEnabled(false);
         }
      } else {
         AppLogger.w("Action bar is null");
      }
   }

   public void setupToolbar() {
      if (mToolbar != null) {
         setSupportActionBar(mToolbar);
      }
   }

   public void showToolbar(boolean isShow) {
      if (mToolbar != null) {
         if (isShow) {
            mToolbar.setVisibility(View.VISIBLE);
         } else {
            AppLogger.d("hide toolbar");
            mToolbar.setVisibility(View.GONE);
         }

      }
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
         case android.R.id.home:
            finish();
            return true;
      }
      return super.onOptionsItemSelected(item);
   }

}
