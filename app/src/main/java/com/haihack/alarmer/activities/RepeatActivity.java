package com.haihack.alarmer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.haihack.alarmer.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class RepeatActivity extends BaseActivity {

   public static final String EXTRA_RESULT = "result";

   @Bind({R.id.groupNoRepeat, R.id.groupEveryday, R.id.groupWeekdays, R.id.groupWeekly, R.id.groupWeekend})
   List<LinearLayout> mGroupPicker;
   @Bind({R.id.rbNoRepeat, R.id.rbEveryday, R.id.rbWeekdays, R.id.rbWeekend, R.id.rbWeekly})
   List<RadioButton> mRadioGroup;
   @Bind({R.id.cbxMon, R.id.cbxTue, R.id.cbxWed, R.id.cbxThu, R.id.cbxFri, R.id.cbxSat, R.id.cbxSun})
   List<CheckBox> mCbxDays;

   /*
    * catch onclick event when click on checkbox
    */
   @OnClick({R.id.groupNoRepeat, R.id.groupEveryday, R.id.groupWeekdays, R.id.groupWeekly, R.id.groupWeekend})
   public void setAlarmDays(LinearLayout layoutGroup) {
      RadioButton selectedRb = (RadioButton) layoutGroup.getChildAt(1);
      selectedRb.setChecked(true);
      int indexSelectedRb = mRadioGroup.indexOf(selectedRb);
      setDayCheckboxes(indexSelectedRb);
      for (int i = 0; i < mRadioGroup.size(); i++) {
         if (i != indexSelectedRb) {
            mRadioGroup.get(i).setChecked(false);
         }
      }
   }

   /*
    * catch onclick event when click on checkbox
    */
   @OnClick({R.id.rbNoRepeat, R.id.rbEveryday, R.id.rbWeekdays, R.id.rbWeekend, R.id.rbWeekly})
   public void setAlarmDays(RadioButton rb) {
      LinearLayout ll = (LinearLayout) rb.getParent();
      setAlarmDays(ll);
   }

   public void setDayCheckboxes(int index) {
      switch (index) {
         case 0:
            for (int i = 0; i < mCbxDays.size(); i++) {
               mCbxDays.get(i).setChecked(false);
               mCbxDays.get(i).setEnabled(false);
            }
            break;
         case 1:
            for (int i = 0; i < mCbxDays.size(); i++) {
               mCbxDays.get(i).setChecked(true);
               mCbxDays.get(i).setEnabled(false);
            }
            break;
         case 2:
            for (int i = 0; i < mCbxDays.size(); i++) {
               mCbxDays.get(i).setEnabled(false);
               if (i < 5) {
                  mCbxDays.get(i).setChecked(true);
               } else {
                  mCbxDays.get(i).setChecked(false);
               }
            }
            break;
         case 3:
            for (int i = 0; i < mCbxDays.size(); i++) {
               mCbxDays.get(i).setEnabled(false);
               if (i > 4) {
                  mCbxDays.get(i).setChecked(true);
               } else {
                  mCbxDays.get(i).setChecked(false);
               }
            }
            break;
         case 4:
            for (int i = 0; i < mCbxDays.size(); i++) {
               mCbxDays.get(i).setEnabled(true);
            }
            break;
      }
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_repeat);
      getSupportActionBar().setTitle("Repeat");
      setAlarmDays(mGroupPicker.get(0));
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
         case android.R.id.home:
            ArrayList<Integer> listDayOrdinals = new ArrayList<>();
            for (int i = 0; i < mCbxDays.size(); i++) {
               if (mCbxDays.get(i).isChecked()) {
                  listDayOrdinals.add(i);
               }
            }

            Intent returnIntent = new Intent();
            if (listDayOrdinals.size() > 0) {
               returnIntent.putIntegerArrayListExtra(EXTRA_RESULT, listDayOrdinals);
               setResult(Activity.RESULT_OK, returnIntent);
            } else {
               setResult(Activity.RESULT_CANCELED, returnIntent);
            }
            finish();
            break;
      }
      return super.onOptionsItemSelected(item);
   }

}
