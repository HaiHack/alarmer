package com.haihack.alarmer.db;

/**
 * Created by haihack on 02/02/2016.
 */
public class DatabaseConstants {
   public static final String DATABASE_NAME = "myalarm.db";
   public static final int DATABASE_VERSION = 1;
   public static final String[] TABLE_NAME = {"alarm"};

   public static final int TABLE_ALARM = 0;
   public static final String ALARM_ID = "id";
   public static final String ALARM_LABEL = "label";
   public static final String ALARM_RINGTONE_PATH = "ringtone";
   public static final String ALARM_DIFFICULTY = "difficulty";
   public static final String ALARM_REPEAT_DAYS = "days";
   public static final String ALARM_IS_VIBRATE = "vibrate";
   public static final String ALARM_IS_ACTIVE = "active";
   public static final String ALARM_GOES_OFF_TIME = "time";


   public static final String CREATE_TABLE_USER = "CREATE TABLE "
         + TABLE_NAME[0] + " ("
         + ALARM_ID + " integer primary key, "
         + ALARM_LABEL + " text, "
         + ALARM_GOES_OFF_TIME + " text, "
         + ALARM_RINGTONE_PATH + " text, "
         + ALARM_DIFFICULTY + " integer, "
         + ALARM_REPEAT_DAYS + " blob, "
         + ALARM_IS_VIBRATE + " integer, "
         + ALARM_IS_ACTIVE + " integer)";
}
