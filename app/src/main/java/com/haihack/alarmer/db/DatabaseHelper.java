package com.haihack.alarmer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.haihack.alarmer.models.Alarm;
import com.haihack.alarmer.utilities.AppLogger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by haihack on 02/02/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

   private static DatabaseHelper sInstance = null;
   private Context mContext;

   private DatabaseHelper(Context context) {
      super(context, DatabaseConstants.DATABASE_NAME, null,
            DatabaseConstants.DATABASE_VERSION);
      mContext = context;
   }

   public static DatabaseHelper getInstance(Context context) {
      if (sInstance == null) {
         sInstance = new DatabaseHelper(context.getApplicationContext());
      }
      return sInstance;
   }

   @Override
   public void onCreate(SQLiteDatabase db) {
      db.execSQL(DatabaseConstants.CREATE_TABLE_USER);
   }

   @Override
   public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      db.execSQL("drop table if exists " + DatabaseConstants.CREATE_TABLE_USER);
   }

   // ==========Method============
   public ContentValues getContentValuesFromData(Object obj, int table) {
      ContentValues cv = new ContentValues();
      switch (table) {
         case DatabaseConstants.TABLE_ALARM:
            Alarm alarm = (Alarm) obj;
            cv.put(DatabaseConstants.ALARM_LABEL, alarm.getLabel());
            if (alarm.getRingtoneUri() != null) {
               cv.put(DatabaseConstants.ALARM_RINGTONE_PATH, alarm.getRingtoneUri().toString());
            }
            cv.put(DatabaseConstants.ALARM_GOES_OFF_TIME, alarm.getGoesOffTimeInString());
            cv.put(DatabaseConstants.ALARM_DIFFICULTY, alarm.getDifficulty().ordinal());
            cv.put(DatabaseConstants.ALARM_DIFFICULTY, alarm.getDifficulty().ordinal());
            try {
               ByteArrayOutputStream bos = new ByteArrayOutputStream();
               ObjectOutputStream oos = new ObjectOutputStream(bos);
               oos.writeObject(alarm.getRepeatDays());
               byte[] byteArray = bos.toByteArray();
               cv.put(DatabaseConstants.ALARM_REPEAT_DAYS, byteArray);
            } catch (IOException e) {
               AppLogger.e(e.getMessage());
            }
            cv.put(DatabaseConstants.ALARM_IS_VIBRATE, alarm.isVibrate() ? 1 : 0);
            cv.put(DatabaseConstants.ALARM_IS_ACTIVE, alarm.isActive() ? 1 : 0);
            break;
      }
      return cv;

   }

   public Object getDataFormCursor(Cursor c, int table) {
      switch (table) {
         case DatabaseConstants.TABLE_ALARM:
            Alarm alarm = new Alarm();
            alarm.setId(c.getInt(c.getColumnIndex(DatabaseConstants.ALARM_ID)));
            alarm.setLabel(c.getString(
                  c.getColumnIndex(DatabaseConstants.ALARM_LABEL)));
            String uriString = c.getString(c.getColumnIndex(DatabaseConstants.ALARM_RINGTONE_PATH));
            if (uriString != null) {
               alarm.setRingtoneUri(Uri.parse(uriString));
            }
            alarm.setGoesOffTimeInString(c.getString(
                  c.getColumnIndex(DatabaseConstants.ALARM_GOES_OFF_TIME)));
            alarm.setIsActive((c.getInt(
                  c.getColumnIndex(DatabaseConstants.ALARM_IS_ACTIVE))) == 1);
            alarm.setIsVibrate((c.getInt(
                  c.getColumnIndex(DatabaseConstants.ALARM_IS_VIBRATE))) == 1);
            alarm.setDifficulty(Alarm.Difficulty.values()[c.getInt(
                  c.getColumnIndex(DatabaseConstants.ALARM_DIFFICULTY))]);

            byte[] repeatDaysBytes = c.getBlob(c.getColumnIndex(DatabaseConstants.ALARM_REPEAT_DAYS));
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(repeatDaysBytes);
            try {
               ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
               Alarm.Day[] repeatDays;
               Object object = objectInputStream.readObject();
               if (object instanceof Alarm.Day[]) {
                  repeatDays = (Alarm.Day[]) object;
                  alarm.setRepeatDays(repeatDays);
               }
            } catch (IOException | ClassNotFoundException e) {
               e.printStackTrace();
            }

            return alarm;
      }
      return null;
   }

   public long insertData(Object obj, int table) {
      SQLiteDatabase db = this.getWritableDatabase();
      //db.execSQL("PRAGMA foreign_keys=ON;");
      ContentValues cv = getContentValuesFromData(obj, table);
      return db.insert(DatabaseConstants.TABLE_NAME[table], null, cv);
   }

   /**
    * Get record by Id
    *
    * @return
    */
   public Object getData(int objectID, int table) {
      SQLiteDatabase db = this.getReadableDatabase();
      String query = "";
      switch (table) {
         case DatabaseConstants.TABLE_ALARM:
            query = "select * from " + DatabaseConstants.TABLE_NAME[table] + " where "
                  + DatabaseConstants.ALARM_ID + " = " + objectID;
      }
      Cursor c = db.rawQuery(query, null);
      if (c.getCount() == 1) {
         c.moveToFirst();
      } else
         return null;
      return getDataFormCursor(c, table);
   }

   public List<Object> getDataFromQuery(int table, String query) {
      List<Object> list = new ArrayList<>();

      SQLiteDatabase db = this.getReadableDatabase();
      Cursor c = db.rawQuery(query, null);
      if (c.moveToFirst()) {
         do {
            list.add(getDataFormCursor(c, table));
         } while (c.moveToNext());

      }
      return list;
   }

   public List<Object> getAllData(int table) {
      String query = "select * from " + DatabaseConstants.TABLE_NAME[table];
      return getDataFromQuery(table, query);
   }

   public int updateData(Object obj, int table) {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues values = getContentValuesFromData(obj, table);
      switch (table) {
         case DatabaseConstants.TABLE_ALARM:
            if (obj instanceof Alarm) {
               Alarm user = (Alarm) obj;
               return db.update(DatabaseConstants.TABLE_NAME[table], values,
                     DatabaseConstants.ALARM_ID + " = ?",
                     new String[]{String.valueOf(user.getId())});
            }
            break;
      }
      return -1;
   }

   public int deleteTable(int table) {
      SQLiteDatabase db = this.getWritableDatabase();
      return db.delete(DatabaseConstants.TABLE_NAME[table], null, null);
   }

   public int deleteData(int id, int table) {
      SQLiteDatabase db = this.getWritableDatabase();
      switch (table) {
         case DatabaseConstants.TABLE_ALARM:
            return db.delete(DatabaseConstants.TABLE_NAME[table], DatabaseConstants.ALARM_ID
                  + " = ?", new String[]{String.valueOf(id)});
      }
      return -1;
   }

   public void closeDB() {
      SQLiteDatabase db = this.getReadableDatabase();
      if (db != null && db.isOpen()) {
         db.close();
      }
   }

   public Cursor getCursorObj(String id, int table) {
      SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
      queryBuilder.setTables(DatabaseConstants.TABLE_NAME[table]);
      Cursor c = null;
      switch (table) {
//         case DatabaseConstants.TABLE_ALARM:
//            c = queryBuilder.query(this.getReadableDatabase(), new String[] {
//                        "id", DatabaseConstants.BOOK_NAME }, "id = ?", new String[] { id },
//                  null, null, null, "1");
//            break;
//         default:
//            break;
      }
      return c;

   }

}
