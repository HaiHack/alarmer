package com.haihack.alarmer.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.haihack.alarmer.activities.MainActivity;
import com.haihack.alarmer.db.DatabaseConstants;
import com.haihack.alarmer.db.DatabaseHelper;
import com.haihack.alarmer.models.Alarm;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by haihack on 06/04/2016.
 */
public class RingtonePlayingService extends IntentService {
   private NotificationManager mAlarmNotificationManager;

   public RingtonePlayingService() {
      super("AlarmService");
   }

   @Override
   public void onHandleIntent(Intent intent) {
//      sendNotification("Wake Up! Wake Up!");
//      Alarm alarm = getNext();
//      if(alarm != null){
//         alarm.schedule(getApplicationContext());
//         //Log.d(this.getClass().getSimpleName(), alarm.getTimeUntilNextAlarmMessage());
//
//      }else{
//         Intent myIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
//         myIntent.putExtra("alarm", Parcels.wrap(new Alarm()));
//
//         PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent,PendingIntent.FLAG_CANCEL_CURRENT);
//         AlarmManager alarmManager = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
//
//         alarmManager.cancel(pendingIntent);
//      }

   }

   private void sendNotification(String msg) {
      mAlarmNotificationManager = (NotificationManager) this
            .getSystemService(Context.NOTIFICATION_SERVICE);

      PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
            new Intent(this, MainActivity.class), 0);

      NotificationCompat.Builder alamNotificationBuilder = new NotificationCompat.Builder(
            this).setContentTitle("Alarm").setSmallIcon(android.R.drawable.ic_menu_add)
            .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
            .setContentText(msg);

      alamNotificationBuilder.setContentIntent(contentIntent);
      mAlarmNotificationManager.notify(1, alamNotificationBuilder.build());
   }

   private Alarm getNext(){
      Set<Alarm> alarmQueue = new TreeSet<>(new Comparator<Alarm>() {
         @Override
         public int compare(Alarm a1, Alarm rhs) {
            int result = 0;
            long diff = a1.getGoesOffTime().getTimeInMillis() - rhs.getGoesOffTime().getTimeInMillis();
            if(diff>0){
               return 1;
            }else if (diff < 0){
               return -1;
            }
            return result;
         }
      });

      DatabaseHelper db = DatabaseHelper.getInstance(getApplicationContext());
      List<Alarm> alarms = (List<Alarm>) (List<?>) db.getAllData(DatabaseConstants.TABLE_ALARM);

      for(Alarm alarm : alarms){
         if(alarm.isActive())
            alarmQueue.add(alarm);
      }
      if(alarmQueue.iterator().hasNext()){
         return alarmQueue.iterator().next();
      }else{
         return null;
      }
   }
}
