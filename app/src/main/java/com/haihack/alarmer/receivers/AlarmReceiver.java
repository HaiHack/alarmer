package com.haihack.alarmer.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.haihack.alarmer.activities.MathSlovingActivity;

/**
 * Created by haihack on 06/04/2016.
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {
   public static final String EXTRA_ALARM = "alarm";
   @Override
   public void onReceive(Context context, Intent intent) {
//      Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
//      if (alarmUri == null) {
//         alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//      }
//      try {
//         MediaPlayer mp = new MediaPlayer();
//         mp.setDataSource(context, alarmUri);
//         final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//         if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
//            mp.setAudioStreamType(AudioManager.STREAM_RING);
//            mp.setLooping(true);
//            mp.prepare();
//            mp.start();
//         }
//      } catch (IOException e) {
//         e.printStackTrace();
//      }

//      Ringtone ringtone = RingtoneManager.getRingtone(context, alarmUri);
//      ringtone.play();

//      String state = intent.getExtras().getString("extra");
//      Log.e("MyActivity", "In the receiver with " + state);

//      String richard_id = intent.getExtras().getString("quote id");
//      Log.e("Richard quote is" , richard_id);

//      Intent serviceIntent = new Intent(context, RingtonePlayingService.class);
//      serviceIntent.putExtra("extra", state);
//      serviceIntent.putExtra("quote id", richard_id);

//      context.startService(serviceIntent);
//      intent.getExtras();
//      intent.get
//      intent.putExtra

      Intent i = new Intent(context, MathSlovingActivity.class);
      i.putExtras(intent.getExtras());
      i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(i);
   }

}
