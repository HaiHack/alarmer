package com.haihack.alarmer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.haihack.alarmer.R;
import com.haihack.alarmer.db.DatabaseConstants;
import com.haihack.alarmer.db.DatabaseHelper;
import com.haihack.alarmer.models.Alarm;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by haihack on 28/03/2016.
 */
public class AlarmAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
   private List<Alarm> mData = new ArrayList<>();
   private Context mContext;
   private DatabaseHelper mDb;

   public AlarmAdapter(Context c, List<Alarm> listAlarms) {
      this.mData = listAlarms;
      mContext = c;
      mDb = DatabaseHelper.getInstance(c);
   }

   @Override
   public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(mContext).inflate(R.layout.item_alarm, parent, false);
      return new AlarmViewHolder(view);
   }

   @Override
   public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
      AlarmViewHolder vh = (AlarmViewHolder) holder;
      final Alarm alarm = mData.get(position);
      vh.mTvTime.setText(alarm.getGoesOffTimeInString());
      if (alarm.getRepeatDays() != null) {
         vh.mTvDays.setText(alarm.getRepeatDaysInString());
         vh.mTvDays.setVisibility(View.VISIBLE);
      }else{
         vh.mTvDays.setVisibility(View.GONE);
      }
      vh.mSwitchAlarm.setChecked(alarm.isActive());
      vh.mSwitchAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
         @Override
         public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            alarm.setIsActive(b);
            mDb.updateData(alarm, DatabaseConstants.TABLE_ALARM);
            if (b) {
               alarm.schedule(mContext);
            } else {
               alarm.cancelSchedule(mContext);
            }
         }
      });
   }

   @Override
   public int getItemCount() {
      return mData.size();
   }

   public static class AlarmViewHolder extends RecyclerView.ViewHolder {
      @Bind(R.id.switchAlarm)
      SwitchCompat mSwitchAlarm;
      @Bind(R.id.tvDays)
      TextView mTvDays;
      @Bind(R.id.tvTime)
      TextView mTvTime;

      public AlarmViewHolder(View view) {
         super(view);
         ButterKnife.bind(this, view);
      }
   }

}
